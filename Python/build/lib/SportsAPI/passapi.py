"""
Name: Sports Analytics APIs
Abbreviation: SAPI
Filename: sapi.py
Author: Jacob Zorn
Version: 0.5.0
Date Updated: 09/27/2019
-------------------------------------------------
Licensing:
Licensed by GPL for Penn State Sports Analytics
and Sabermetrics Clubs.
Licensed by MIT for outside organizations.
Both license texts are available on the repo where
this code is hosted. 
 -------------------------------------------------
Functions:
Get_JSON_Game_File (Method, GameID, SaveLocation='', Sport='', League='')
	- Method:
		You can choose to get the JSON file from a previously
		saved location (provided in SaveLocation) or via
		a publicly available API.
			- Currently only the hidden ESPN API is available
	- GameID:
		Provide the GameID for a given sport game
	- Sport:
		Such as 'fb','bb','h','bbk','soc'
	- League:
		The necessary league the game is in.
		Example: 'ncaaf','nfl','ncaabb',...
	- Outputs a dictionary in JSON file.
	
Save_JSON (Game_Data, SaveLocation)
	- Game_Data JSON data recieved from Get_JSON_File
	- Saves in the SaveLocation you provide
	- No Output

CFB_Box_Score (Game_Data)
	- Game_Data is the JSON data you receive from Get_JSON_File
	- Returns the box score for a given game
	- Outputs Home_Stats, Away_Stats, Game_Stats
	- Game_Stats is the general game data such as stat categories, and other
	  game info.
	
Box_Score_Printer (Home_Stats, Away_Stats, Game_Data, SaveLocation)
	- Prints the Box Score Data to a file that you provide in SaveLocation
	- No Output
	
Get_Request (API_URL)
	- Gets an API Request
	- API_URL the url to the necessary get request
	
Create_URL (Sport, League, Query, Argument='')
	- Creates the necessary API_URL used by the Get_Request
	- You won't have to access this code. It is built for the 
	  Get_JSON_File routine
	- Only currently works for the Hidden ESPN API
		- Will update for other APIs

Get_Team_List()
	- Gets a list of teams from the Hidden ESPN API that is necessary
	  at times to ensure you use the correct team name or id
	- Outputs a JSON dictionary with the necessary Teams information
	- Output can be saved using Save_JSON

Season_Scraper (URL)
	- Uses Beautiful Soup to parse and scrape a web page to get schedule
	  data from a given url
	  
Hello ()
	- Just prints out that this API resource has loaded correctly.
	
Get_Recruiting_Data (Year, EndYear='')
	- EndYear is optional depending on if you want a series of years
	- Year is the year you want to get data on or the start of a 
	  series of years. 
	- Data comes from CollegeFootballData API
	- Outputs a JSON dictionary that can be saved using Save_JSON
 -------------------------------------------------
 Examples of how to use the code can be seen in the 
 README file.
"""

#--------------------------------------------------
#Import Necessary Modules
import requests, json, time, textwrap
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
import os
import pandas as pd

#--------------------------------------------------
#Routines

def Hello():
	stringtoprint = 'Hidden ESPN API has been loaded. Refer to documentation'
	stringtoprint += ' and README for more information on using this API'
	stringtoprint += ' service.'
	
	for stn in textwrap.wrap(stringtoprint,60):
		print(stn)

def Create_URL(Sport, League, Query, Argument=''):
	
	sport_values 	= ['football', 'baseball', 'hockey', 'basketball',
					'soccer']
	sport_tags 		= ['fb','bb','h','bbk','soc']
	league_tags 	= ['ncaaf','nfl','ncaabb','mlb','nhl',
					'nba','wnba','ncaam','ncaaw','epl','mls']
	leagues 		= ['college-football', 'nfl',
						'college-baseball','mlb',
						'nhl','nba','wnba',
						'womens-college-basketball',
						'mens-college-basketball','eng.1','usa.1']
	sports_dict = dict(zip(sport_tags,sport_values))
	leagues_dict = dict(zip(league_tags,leagues))
	
	query_tags 		= ['news', 'scoreboard', 'game', 'team', 'rankings']
	query_values 	= ['news', 'scoreboard', 'summary', 'teams', 'rankings']
	query_dict		= dict(zip(query_tags, query_values))
	
	url_sport	= ''
	url_league 	= ''
	url_query 	= ''
	for i in sports_dict.keys():
		if i == Sport.lower():
			url_sport = sports_dict[i]
	for i in leagues_dict.keys():
		if i == League.lower():
			url_league = leagues_dict[i]
	for i in query_dict.keys():
		if i == Query.lower():
			url_query = query_dict[i]
	
	prefix 	= 'http://site.api.espn.com/apis/site/v2/sports/'
	url 	= prefix + url_sport + '/' + url_league + '/'
	url 	+= url_query
	
	if Argument != '':
		#This means that we have a teamID or gameID or Date
		if Query.lower() == 'game':
			url += '?event=' + str(Argument)
		if Query.lower() == 'team':
			url += '/' + str(Argument)
		if Query.lower() == 'scoreboard':
			url += '?dates=' + str(Argument)
			
	return url

def Get_Request(API_URL):
	
	
	iflag = 0
	while iflag == 0:
		response = requests.get(API_URL)
		if response.status_code != 200:
			iflag = 0
			print('The GET Request return unsuccessfully. Getting again.')
			time.sleep(4)
		else:
			iflag = 1
	
	return_data = json.loads(response.text)
	
	return return_data
	
def Get_Team_List(Sport, League):
	
	url = Create_URL(Sport, League, 'team')
	url = url + '?limit=900'
	json_data = Get_Request(url)

	return json_data['sports'][0]['leagues'][0]['teams']
	
def Save_JSON(Data, SaveLocation):
	
	with open(SaveLocation,'w') as outfile:
		json.dump(Data, outfile)
		
def Get_JSON_Game_File(Method, GameID='', SaveLocation='', Sport='', League=''):

	if Method.lower() == 'api':
		api_url = Create_URL(Sport, League, 'game',Argument=GameID)
		print(api_url)
		game_data = Get_Request(api_url)
		if SaveLocation != '':
			Save_JSON(game_data,SaveLocation)
	if Method.lower() == 'file':
		with open(SaveLocation) as json_file:
			game_data = json.load(json_file)
	
	return game_data
	
def Save_PlaybyPlay(Plays, SaveLocation, GameID):
	OutputFile = os.path.join(SaveLocation, (str(GameID)+'PbP'+'.csv'))
	
	save_df = pd.DataFrame(Plays)
	save_df.to_csv(OutputFile,index=False)

def Get_Recruiting_Data(Year,EndYear=''):
	
	if EndYear == '':
		EndYear = Year

	url = 'https://api.collegefootballdata.com/recruiting/groups?startYear={}&endYear={}'.format(str(Year),str(EndYear))

	data = Get_Request(url)
	
	return data
	
def Season_Scraper(URL):

	iflag = 0

	soup = bs(urlopen(URL),'html.parser')

	# while iflag == 0:
		# try:
			# soup = bs(urlopen(URL),'html.parser')
			# iflag == 1
		# except:
			# iflag == 0
			
	tables = soup.find_all('table',{'class':'Table'})[0]

	table_rows = tables.find_all('tr')
	schedule_data = []
	gameID_dict = {'gameID':[]}

	for row in table_rows:
		temp_data = []
		for td in row.find_all('td'):
			#See if the data piece has the 'a' tag
			if len(td.find_all('a')) > 0:
				a_tag = td.find_all('a')[0].get('href')
				if 'gameId' in a_tag:
					temp_data.append(a_tag)
				else:
					temp_data.append(td.text)
		schedule_data.append(temp_data)
		
	for game in schedule_data:
		for item in game:
			if 'gameId' in item:
				gameID_dict['gameID'].append(item[(item.find('gameId')+7):])

	return gameID_dict

def Season_Scraper_URLs(TeamID, League, Seasons):

	league_tags = ['nfl','ncaaf','mlb','ncaabb','ncaam','ncaaw','nba',
					'wnba','nhl']
	league_values = ['nfl','college-football','mlb','college-baseball',
					'mens-college-basketball','womens-college-basketball',
					'nba','wnba','nhl']
	league_dict = dict(zip(league_tags,league_values))

	prefix = 'https://www.espn.com/' + str(league_dict[League]) + '/team/schedule/_/id/'
	season_fix = '/season/'

	if type(Seasons) is not list:
		Seasons = [Seasons]
		
	season_dict = {}

	for season in Seasons:
		print(season)
		if League != 'nhl':
			url = prefix + str(TeamID) + season_fix + str(season)
		else:
			url = prefix + str(TeamID) + season_fix + str(season) + '/seasontype/2'
		print(url)
		gameIDs = Season_Scraper(url)
		season_dict[season] = gameIDs
		time.sleep(2)
	
	return season_dict
	
class CFB:

	def Season_Scraper_URLs(TeamID,Seasons):
	
		prefix = 'https://www.espn.com/college-football/team/schedule/_/id/' 
		season_fix = '/season/'
		
		if type(Seasons) is not list:
			Seasons = [Seasons]
		
		season_dict = {}
		
		for season in Seasons:
			url = prefix + str(TeamID) + season_fix + str(season)
			gameIDs = Season_Scraper(url)
			season_dict[season] = gameIDs
			
		return season_dict

	def Box_Score(Game_Data):
		
		boxscore	= Game_Data['boxscore']
		teams		= boxscore['teams']
		away_team	= teams[0]
		home_team	= teams[1]
		away_stats	= away_team['statistics']
		home_stats	= home_team['statistics']
		away_data	= away_team['team']
		home_data	= home_team['team']
		
		stt_cat = [];	h_stats = []; a_stats = []
		
		for cat in range(len(away_stats)):
			stt_cat.append(home_stats[cat]['name'])
			h_stats.append(home_stats[cat]['displayValue'])
			a_stats.append(away_stats[cat]['displayValue'])
			
		game_cat = ['season','homeID','Home Team','awayID','Away Team']
		game_dat = [Game_Data['header']['season']['year'],home_data['id'],home_data['location'],away_data['location'],away_data['id']]
		
		return dict(zip(stt_cat,h_stats)), dict(zip(stt_cat,a_stats)), dict(zip(game_cat,game_dat))
		
	def Box_Score_Printer(Home_Stats, Away_Stats, Game_Data, SaveLocation):

		home_team 	= Game_Data['Home Team']
		away_team 	= Game_Data['Away Team']
		season		= Game_Data['season']
		
		file_name = SaveLocation
		
		with open(file_name,'w') as outfile:
			outfile.write('Box Score for ' + away_team + ' vs ' + home_team + ' in the ' + str(season) + ' Season.')
			outfile.write('Stat' + '\t' + 'Away Team' + '\t' + 'Home Team' + '\n')
			for stat in Home_Stats.keys():
				outfile.write(str(stat) + '\t' + str(Away_Stats[stat]) + '\t' + str(Home_Stats[stat]) + '\n')

		return ('Finishing Writing to ' + file_name)
	
	def Parse_Drives(JSON_Data):
	
		master_plays = []
		labels = ['HomeID','AwayID','HomeName','AwayName','GameDate','GameID','Season','NeutralSite']
		
		home_team_id	= JSON_Data['boxscore']['teams'][1]['team']['id']
		away_team_id	= JSON_Data['boxscore']['teams'][0]['team']['id']
		game_date		= JSON_Data['header']['competitions'][0]['date']
		season			= JSON_Data['header']['season']['year']
		gameID			= JSON_Data['header']['id']
		neutral_site	= JSON_Data['header']['competitions'][0]['neutralSite']
		home_name		= JSON_Data['boxscore']['teams'][1]['team']['abbreviation']
		away_name		= JSON_Data['boxscore']['teams'][0]['team']['abbreviation']

		drive_count = 0
		
		for drive in JSON_Data['drives']['previous']:
			for key in drive.keys():
				if key != 'plays':
					if type(drive[key]) is dict:
						for kky in drive[key].keys():
							if kky != 'logos':
								if type(drive[key][kky]) is dict:
									for kkky in drive[key][kky].keys():
										label = ((str(key)+str(kky)+str(kkky)))
										if label not in labels:
											labels.append(label)
								else:
									label = key + kky
									if label not in labels:
										labels.append(label)
					elif type(drive[key]) is list:
						print('This is a list')
					else:
						if key not in labels:
							labels.append(key)
			
		for drive in JSON_Data['drives']['previous']:
			temp_drive = {}
			for label in labels:
				temp_drive[label] = 'None'
			temp_drive['HomeID'] = home_team_id
			temp_drive['HomeName'] = home_name
			temp_drive['AwayID'] = away_team_id
			temp_drive['AwayName'] = away_name
			temp_drive['GameDate'] = game_date
			temp_drive['GameID'] = gameID
			temp_drive['NeutralSite'] = neutral_site
			temp_drive['Season'] = season
			for key in drive.keys():
				if key != 'plays':
					if type(drive[key]) is dict:
						for kky in drive[key].keys():
							if kky != 'logos':
								if type(drive[key][kky]) is dict:
									for kkky in drive[key][kky].keys():
										label = (str(key) + str(kky) + str(kkky))
										temp_drive[label] = drive[key][kky][kkky]
								else:
									label = key + kky
									temp_drive[label] = drive[key][kky]
					elif type(drive[key]) is list:
						print('This is a list')
					else:
						temp_drive[key] = drive[key]
							
			master_plays.append(temp_drive)
		return master_plays
	
	def Parse_PlaybyPlay(JSON_Data):
		
		master_plays = []
		labels = ['HomeID','AwayID','GameDate','GameID','Season','NeutralSite']
		
		home_team_id	= JSON_Data['boxscore']['teams'][1]['team']['id']
		away_team_id	= JSON_Data['boxscore']['teams'][0]['team']['id']
		game_date		= JSON_Data['header']['competitions'][0]['date']
		season			= JSON_Data['header']['season']['year']
		gameID			= JSON_Data['header']['id']
		neutral_site	= JSON_Data['header']['competitions'][0]['neutralSite']

		player_count = 0
		play_count = 0
		
		for drive in JSON_Data['drives']['previous']:
			for play in drive['plays']:
				for key in play.keys():
					if type(play[key]) is dict:
						for cat in play[key].keys():
							if type(play[key][cat]) is dict:
								for kky in play[key][cat].keys():
									label = ((str(key)+str(cat)))
									if label not in labels:
										labels.append(label)
							else:
								label = key+cat
								if label not in labels:
									labels.append(label)
					elif type(play[key]) is list:
						for player in range(len(play[key])):
							for kky in play[key][player].keys():
								if type(play[key][player][kky]) is dict:
									for kkky in play[key][player][kky].keys():
										if kkky != 'headshot':
											label = 'Player'+str(player+1)+kkky
											if label not in labels:
												labels.append(label)
								else:
									label = 'Player'+str(player+1)+kky
									if label not in labels:
										labels.append(label)
					else:
						if key not in labels:
							labels.append(key)
							
		for drive in JSON_Data['drives']['previous']:
			for play in drive['plays']:
				temp_play = {}
				for label in labels:
					temp_play[label] = 'None'
				temp_play['HomeID'] 	= home_team_id
				temp_play['AwayID'] 	= away_team_id
				temp_play['GameDate']	= game_date
				temp_play['GameID']		= gameID
				temp_play['NeutralSite']= neutral_site
				temp_play['Season']		= season
				for key in play.keys():
					if type(play[key]) is dict:
						for cat in play[key].keys():
							if type(play[key][cat]) is dict:
								for kky in play[key][cat].keys():
									label = ((str(key)+str(cat)))
									temp_play[label] = play[key][cat][kky]
							else:
								label = key+cat
								temp_play[label] = play[key][cat]
					elif type(play[key]) is list:
						for player in range(len(play[key])):
							for kky in play[key][player].keys():
								if type(play[key][player][kky]) is dict:
									for kkky in play[key][player][kky].keys():
										if kkky != 'headshot':
											label = 'Player'+str(player+1)+kkky
											label = 'Player'+str(player+1)+kkky
											temp_play[label] = play[key][player][kky][kkky]
								else:
									label = 'Player'+str(player+1)+kky
									temp_play[label] = play[key][player][kky]
					else:
						temp_play[key] = play[key]
				master_plays.append(temp_play)
	
		return master_plays
	
	def Save_PlaybyPlay(Plays, SaveLocation, GameID):
		OutputFile = os.path.join(SaveLocation, (str(GameID)+'PbP'+'.csv'))
		
		save_df = pd.DataFrame(Plays)
		save_df.to_csv(OutputFile,index=False)
			
class NFL:
	
	def Parse_PlaybyPlay(JSON_Data):
		
		master_plays = []
		labels = ['HomeID','AwayID','GameDate','GameID','Season','NeutralSite']
		
		home_team_id	= JSON_Data['boxscore']['teams'][1]['team']['id']
		away_team_id	= JSON_Data['boxscore']['teams'][0]['team']['id']
		game_date		= JSON_Data['header']['competitions'][0]['date']
		season			= JSON_Data['header']['season']['year']
		gameID			= JSON_Data['header']['id']
		neutral_site	= JSON_Data['header']['competitions'][0]['neutralSite']

		player_count = 0
		play_count = 0
		
		for drive in JSON_Data['drives']['previous']:
			for play in drive['plays']:
				for key in play.keys():
					if type(play[key]) is dict:
						for cat in play[key].keys():
							if type(play[key][cat]) is dict:
								for kky in play[key][cat].keys():
									label = ((str(key)+str(cat)))
									if label not in labels:
										labels.append(label)
							else:
								label = key+cat
								if label not in labels:
									labels.append(label)
					elif type(play[key]) is list:
						for player in range(len(play[key])):
							for kky in play[key][player].keys():
								if type(play[key][player][kky]) is dict:
									for kkky in play[key][player][kky].keys():
										if kkky != 'headshot':
											label = 'Player'+str(player+1)+kkky
											if label not in labels:
												labels.append(label)
								else:
									label = 'Player'+str(player+1)+kky
									if label not in labels:
										labels.append(label)
					else:
						if key not in labels:
							labels.append(key)
							
		for drive in JSON_Data['drives']['previous']:
			for play in drive['plays']:
				temp_play = {}
				for label in labels:
					temp_play[label] = 'None'
				temp_play['HomeID'] 	= home_team_id
				temp_play['AwayID'] 	= away_team_id
				temp_play['GameDate']	= game_date
				temp_play['GameID']		= gameID
				temp_play['NeutralSite']= neutral_site
				temp_play['Season']		= season
				for key in play.keys():
					if type(play[key]) is dict:
						for cat in play[key].keys():
							if type(play[key][cat]) is dict:
								for kky in play[key][cat].keys():
									label = ((str(key)+str(cat)))
									temp_play[label] = play[key][cat][kky]
							else:
								label = key+cat
								temp_play[label] = play[key][cat]
					elif type(play[key]) is list:
						for player in range(len(play[key])):
							for kky in play[key][player].keys():
								if type(play[key][player][kky]) is dict:
									for kkky in play[key][player][kky].keys():
										if kkky != 'headshot':
											label = 'Player'+str(player+1)+kkky
											label = 'Player'+str(player+1)+kkky
											temp_play[label] = play[key][player][kky][kkky]
								else:
									label = 'Player'+str(player+1)+kky
									temp_play[label] = play[key][player][kky]
					else:
						temp_play[key] = play[key]
				master_plays.append(temp_play)
	
		return master_plays
	
	def Save_PlaybyPlay(Plays, SaveLocation, GameID):
		OutputFile = os.path.join(SaveLocation, (str(GameID)+'PbP'+'.csv'))
		
		save_df = pd.DataFrame(Plays)
		save_df.to_csv(OutputFile,index=False)

	def Box_Score(Game_Data):
		
		boxscore	= Game_Data['boxscore']
		teams		= boxscore['teams']
		away_team	= teams[0]
		home_team	= teams[1]
		away_stats	= away_team['statistics']
		home_stats	= home_team['statistics']
		away_data	= away_team['team']
		home_data	= home_team['team']
		
		stt_cat = [];	h_stats = []; a_stats = []
		
		for cat in range(len(away_stats)):
			stt_cat.append(home_stats[cat]['name'])
			h_stats.append(home_stats[cat]['displayValue'])
			a_stats.append(away_stats[cat]['displayValue'])
			
		game_cat = ['season','homeID','Home Team','awayID','Away Team']
		game_dat = [Game_Data['header']['season']['year'],home_data['id'],home_data['location'],away_data['location'],away_data['id']]
		
		return dict(zip(stt_cat,h_stats)), dict(zip(stt_cat,a_stats)), dict(zip(game_cat,game_dat))
		
	def Box_Score_Printer(Home_Stats, Away_Stats, Game_Data, SaveLocation):

		home_team 	= Game_Data['Home Team']
		away_team 	= Game_Data['Away Team']
		season		= Game_Data['season']
		
		file_name = SaveLocation
		
		with open(file_name,'w') as outfile:
			outfile.write('Box Score for ' + away_team + ' vs ' + home_team + ' in the ' + str(season) + ' Season.')
			outfile.write('Stat' + '\t' + 'Away Team' + '\t' + 'Home Team' + '\n')
			for stat in Home_Stats.keys():
				outfile.write(str(stat) + '\t' + str(Away_Stats[stat]) + '\t' + str(Home_Stats[stat]) + '\n')

		return ('Finishing Writing to ' + file_name)

class NHL:
	
	def Parse_PlaybyPlay(JSON_Data):
		
		master_plays = []
		labels = ['HomeID','AwayID','GameDate','GameID','Season','NeutralSite']
		
		home_team_id	= JSON_Data['boxscore']['teams'][1]['team']['id']
		away_team_id	= JSON_Data['boxscore']['teams'][0]['team']['id']
		game_date		= JSON_Data['header']['competitions'][0]['date']
		season			= JSON_Data['header']['season']['year']
		gameID			= JSON_Data['header']['id']
		neutral_site	= JSON_Data['header']['competitions'][0]['neutralSite']

		player_count = 0
		play_count = 0
		
		for play in JSON_Data['plays']:
			for key in play.keys():
				if type(play[key]) is dict:
					for cat in play[key].keys():
						if type(play[key][cat]) is dict:
							print(play[key][cat],'Is Dict')
						else:
							label = key+cat
							if label not in labels:
								labels.append(label)
				elif type(play[key]) is list:
					for player in range(len(play[key])):
						for kky in play[key][player].keys():
							if type(play[key][player][kky]) is dict:
								for kkky in play[key][player][kky].keys():
									if kkky != 'headshot':
										label = 'Player'+str(player+1)+kkky
										if label not in labels:
											labels.append(label)
							else:
								label = 'Player'+str(player+1)+kky
								if label not in labels:
									labels.append(label)
				else:
					if key not in labels:
						labels.append(key)
		
		for play in JSON_Data['plays']:
			temp_play = {}
			for label in labels:
				temp_play[label] = 'None'
			temp_play['HomeID'] 	= home_team_id
			temp_play['AwayID'] 	= away_team_id
			temp_play['GameDate']	= game_date
			temp_play['GameID']		= gameID
			temp_play['NeutralSite']= neutral_site
			temp_play['Season']		= season
			for key in play.keys():
				if type(play[key]) is dict:
					for cat in play[key].keys():
						if type(play[key][cat]) is dict:
							print(play[key][cat],'Is Dict')
						else:
							label = key + cat
							temp_play[label] = play[key][cat]
							
				elif type(play[key]) is list:
					for player in range(len(play[key])):
						for kky in play[key][player].keys():
							if type(play[key][player][kky]) is dict:
								for kkky in play[key][player][kky].keys():
									if kkky != 'headshot':
										label = 'Player'+str(player+1)+kkky
										temp_play[label] = play[key][player][kky][kkky]
							else:
								label = 'Player'+str(player+1)+kky
								temp_play[label] = play[key][player][kky]
				else:
					temp_play[key] = play[key]
			master_plays.append(temp_play)
			play_count += 1
	

		return master_plays
	
	def Save_PlaybyPlay(Plays, SaveLocation, GameID):
		OutputFile = os.path.join(SaveLocation, (str(GameID)+'PbP'+'.csv'))
		
		save_df = pd.DataFrame(Plays)
		save_df.to_csv(OutputFile,index=False)

	def Box_Score(Game_Data):
		
		boxscore	= Game_Data['boxscore']
		teams		= boxscore['teams']
		away_team	= teams[0]
		home_team	= teams[1]
		away_stats	= away_team['statistics']
		home_stats	= home_team['statistics']
		away_data	= away_team['team']
		home_data	= home_team['team']
		
		home_dict = {}
		away_dict = {}
		
		for cat in range(len(away_stats)):
			home_dict[home_stats[cat]['name']] = (home_stats[cat]['displayValue'])
			away_dict[away_stats[cat]['name']] = (away_stats[cat]['displayValue'])

		home_dict['id']			= home_data['id']
		home_dict['location']	= home_data['location']
		home_dict['season']		= Game_Data['header']['season']['year']
		away_dict['id']			= away_data['id']
		away_dict['location']	= away_data['location']
		away_dict['season']		= Game_Data['header']['season']['year']
		
		return home_dict, away_dict
		
	def Box_Score_Printer(Home_Stats, Away_Stats, Game_Data, SaveLocation):

		home_team 	= Game_Data['Home Team']
		away_team 	= Game_Data['Away Team']
		season		= Game_Data['season']
		
		file_name = SaveLocation
		
		with open(file_name,'w') as outfile:
			outfile.write('Box Score for ' + away_team + ' vs ' + home_team + ' in the ' + str(season) + ' Season.')
			outfile.write('Stat' + '\t' + 'Away Team' + '\t' + 'Home Team' + '\n')
			for stat in Home_Stats.keys():
				outfile.write(str(stat) + '\t' + str(Away_Stats[stat]) + '\t' + str(Home_Stats[stat]) + '\n')

		return ('Finishing Writing to ' + file_name)

	def Player_Box_Score(Game_Data):
		
		boxscore		= Game_Data['boxscore']
		players			= boxscore['players']
		away_players	= players[0]
		home_players	= players[1]
		away_stats		= away_players['statistics']
		home_stats		= home_players['statistics']
		away_team		= away_players['team']['abbreviation']
		home_team		= home_players['team']['abbreviation']
		season			= Game_Data['header']['season']['year']
		
		stat_f_cat 	= ['Season','Name']; stat_f_home	= []; stat_f_away	= []
		stat_d_cat 	= ['Season','Name']; stat_d_home	= []; stat_d_away	= []
		stat_g_cat 	= ['Season','Name']; stat_g_home	= []; stat_g_away	= []
		skater_stats = [];	goalie_stats = []
		
		for i in range(len(away_stats)):
			if i == 0:
			
				for label in away_stats[i]['labels']:
					stat_f_cat.append(label)
					
				for athlete in away_stats[i]['athletes']:
					temp_list = [season,athlete['athlete']['displayName']]
					for stat in athlete['stats']:
						temp_list.append(stat)
					stat_f_away.append(dict(zip(stat_f_cat,temp_list)))
					
				for athlete in home_stats[i]['athletes']:
					temp_list = [season,athlete['athlete']['displayName']]
					for stat in athlete['stats']:
						temp_list.append(stat)
					stat_f_home.append(dict(zip(stat_f_cat,temp_list)))
					
			if i == 1:
			
				for label in away_stats[i]['labels']:
					stat_d_cat.append(label)
					
				for athlete in away_stats[i]['athletes']:
					temp_list = [season,athlete['athlete']['displayName']]
					for stat in athlete['stats']:
						temp_list.append(stat)
					stat_d_away.append(dict(zip(stat_d_cat,temp_list)))
					
				for athlete in home_stats[i]['athletes']:
					temp_list = [season,athlete['athlete']['displayName']]
					for stat in athlete['stats']:
						temp_list.append(stat)
					stat_d_home.append(dict(zip(stat_d_cat,temp_list)))
			if i == 2:

				for label in away_stats[i]['labels']:
					stat_g_cat.append(label)
					
				for athlete in away_stats[i]['athletes']:
					temp_list = [season,athlete['athlete']['displayName']]
					for stat in athlete['stats']:
						temp_list.append(stat)
					stat_g_away.append(dict(zip(stat_g_cat,temp_list)))
					
				for athlete in home_stats[i]['athletes']:
					temp_list = [season,athlete['athlete']['displayName']]
					for stat in athlete['stats']:
						temp_list.append(stat)
					stat_g_home.append(dict(zip(stat_g_cat,temp_list)))
		
		for i in stat_d_home:
			skater_stats.append(i)
		for i in stat_f_home:
			skater_stats.append(i)
		for i in stat_d_away:
			skater_stats.append(i)
		for i in stat_f_away:
			skater_stats.append(i)
		for i in stat_g_away:
			goalie_stats.append(i)
		for i in stat_g_home:
			goalie_stats.append(i)
			
		return skater_stats, goalie_stats
		
class FOX:
	
	def Get_Version(GameID, League=''):
		
		if League == '':
			print('No League Given. Quitting')
			quit()
		if League == 'nfl':
			league_fix = 'nfl'
		if League == 'ncaaf':
			league_fix = 'cfb'
		if League == 'nhl':
			league_fix = 'nhl'
		
		prefix = 'https://api.foxsports.com/sportsdata/v1/live/'
		version_fix = '/version/'
		suffix = '.json?apikey=jE7yBJVRNAwdDesMgTzTXUUSx1It41Fq'
		
		url = prefix + str(league_fix) + version_fix + str(GameID) + suffix
		
		data = Get_Request(url)
		version = data['Number']
		
		return version
		
	def Get_Gamefile(GameID, League=''):
		
		if League == '':
			print('No League Given. Quitting')
			quit()
		if League == 'nfl':
			league_fix = 'nfl'
		if League == 'ncaaf':
			league_fix = 'cfb'
		if League == 'nhl':
			league_fix = 'nhl'
		
		version = FOX.Get_Version(GameID,League=League)
		
		prefix = 'https://api.foxsports.com/sportsdata/v1/live/'
		games_fix = '/games/'
		suffix = 'apikey=jE7yBJVRNAwdDesMgTzTXUUSx1It41Fq'
		version_fix = '.json?version='
		url = prefix + str(league_fix) + games_fix + str(GameID) + version_fix + str(version) + '&' + suffix
		
		data = Get_Request(url)
		
		return json.loads(data.text)

class NHLAPI:
	
	def Get_Gamefile(Season,GameNumber,SeasonType='regular'):
	
		if SeasonType.lower() == 'regular':
			stype = '02'
		elif SeasonType.lower() == 'playoffs':
			stype = '03'
		elif SeasonType.lower() == 'allstar':
			stype = '04'
		else:
			stype = '01'
	
	
		GameID = str(Season) + stype + '{:04d}'.format(GameNumber)
		
		url = 'https://statsapi.web.nhl.com/api/v1/game/{}/feed/live'.format(GameID)
		
		data = Get_Request(url)
		
		return data, GameID
		
	def Parse_BoxScore_Level(Data):
		print('Need to implement')
		
	def Parse_PlaybyPlay(Data):
		
		master_plays = []
		labels = ['HomeTeam','AwayTeam','GameDate','GameID','Season','SeasonType','NeutralSite']
		
		homeTeam 	= Data['gameData']['teams']['home']['name']
		awayTeam 	= Data['gameData']['teams']['away']['name']
		gameDate 	= Data['gameData']['datetime']['dateTime']
		gameID 		= Data['gamePk']
		season 		= Data['gameData']['game']['season']
		seasonType 	= Data['gameData']['game']['type']
		
		try:
			if Data['gameData']['teams']['home']['venue']['id'] != Data['gameData']['venue']['id']:
				neutralSite = True
			else:
				neutralSite = False
		except:
			neutralSite = False
		
		player_count = 0
		play_count = 0
		
		all_plays = Data['liveData']['plays']['allPlays']
		for play in all_plays:
			if 'players' in play.keys():
				for key in play:
					if type(play[key]) is list:
						if key == 'players':
							player_count = 0
						for player in play[key]:
							if key == 'players':
								player_count += 1
							for kkey in player.keys():
								if type(player[kkey]) is dict:
									for kky in player[kkey].keys():
										label = 'Player{:02d}{}'.format(player_count,kky)
										if label not in labels:
											labels.append(label)
								else:
									label = 'Player{:02d}{}'.format(player_count,kkey)
									if label not in labels:
										labels.append(label)
					
					if type(play[key]) is dict:
						for kky in play[key].keys():
							if type(play[key][kky]) is dict:
								for kkey in play[key][kky].keys():
									label = '{}{}{}'.format(key,kky,kkey)
							else:
								label = '{}{}'.format(key,kky)
							if label not in labels:
								labels.append(label)
						else:
							label = '{}'.format(key)
							
		for play in all_plays:
			if 'players' in play.keys():
				temp_play = {}
				for label in labels:
					temp_play[label] = 'None'
				
				temp_play['HomeTeam'] = homeTeam
				temp_play['AwayTeam'] = awayTeam
				temp_play['gameDate'] = gameDate
				temp_play['GameID'] = gameID
				temp_play['Season'] = season
				temp_play['SeasonType'] = seasonType
				temp_play['NeutralSite'] = neutralSite
				if 'players' in play.keys():
					for key in play:
						if type(play[key]) is list:
							if key == 'players':
								player_count = 0
							for player in play[key]:
								if key == 'players':
									player_count += 1
								for kkey in player.keys():
									if type(player[kkey]) is dict:
										for kky in player[kkey].keys():
											label = 'Player{:02d}{}'.format(player_count,kky)
											temp_play[label] = player[kkey][kky]
									else:
										label = 'Player{:02d}{}'.format(player_count,kkey)
										temp_play[label] = player[kkey]
						
						if type(play[key]) is dict:
							for kky in play[key].keys():
								if type(play[key][kky]) is dict:
									for kkey in play[key][kky].keys():
										label = '{}{}{}'.format(key,kky,kkey)
										temp_play[label] = play[key][kky][kkey]
								else:
									label = '{}{}'.format(key,kky)
									temp_play[label] = play[key][kky]
							else:
								label = '{}'.format(key)
				master_plays.append(temp_play)
			
		return master_plays
		
#---------------------------------------------------
#Main Code

Hello()
	
	
	
	
	
	
	
	
	
	
	
	